============
Contributing
============

Every enhancement, fix or suggestion to Open Banking is greatly appreciated and credit will always be given.