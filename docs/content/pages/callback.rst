Call Back URL
#############

In the authorisation request if the request_type = "code" then you will see a ? (query parameter).
If it is code id_token which is the recommended hybrid flow you will have the # (URL fragment).
Some ASPSPs support both, some don't, so as a TPP you will need to build solutions to handle both possible scenarios.




Why use a fragment?
-------------------
The URI Fragment is used instead of query parameter, from a security point of view.
The URI segment will never be sent over the network to the redirect URL. For e.g. after login on
the Oauth Authorization server, the location header will have `{{ur redirect url"#access_token=uraccesstoken}}`
and the response code will be 302. When the browser sees the 302, it will redirect to the location header value
automatically (the user agent does it automatically and the javascript cannot stop this (afaik).

Since its a URI fragment, only the redirect URL is sent across the network, the URL fragment is not.
If it was a query parameter, the query parameter will also be sent over the network. Even with TLS,
the query parameter will be visible in your proxy logs, making the access token known to unintended people,
causing a leak of the access token.