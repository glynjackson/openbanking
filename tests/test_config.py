"""
Tests for config.
"""

from openbanking.config import model_banks

def test_no_duplicated_banks():
    """
    Sanity test making sure I haven't done something stupid to the config.
    """
    pass