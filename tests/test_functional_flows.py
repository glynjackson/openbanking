"""
Functional tests for dynamic registration, account requests, token access and account/transaction endpoints.

"""
import os
import pytest
import webbrowser
from openbanking import OpenBanking
from openbanking.config import model_banks

import logging

#
# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
try:
    import http.client as http_client
except ImportError:
    # Python 2
    import httplib as http_client
http_client.HTTPConnection.debuglevel = 1

# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True


def get_psu_input_code(pytestconfig, bank=None, ) -> (str, str):
    """ A function to suspend the test while we wait for psu to input code. """
    capture_manager = pytestconfig.pluginmanager.getplugin("capturemanager")
    capture_manager.suspend_global_capture(in_=True)
    psu_input_code = input("\n\nEnter code for {}: ".format(bank))
    psu_input_id_token = input("Enter id_token for {} if you have one: ".format(bank))
    capture_manager.resume_global_capture()
    return psu_input_code, psu_input_id_token


def display_something(pytestconfig, message=None, ) -> str:
    """ A function to suspend the test and display a message. """
    capture_manager = pytestconfig.pluginmanager.getplugin("capturemanager")
    capture_manager.suspend_global_capture(in_=True)
    psu_display = input("\n\nOutput {}: ".format(message))
    capture_manager.resume_global_capture()
    return psu_display


@pytest.mark.skipif(os.environ.get("LOCAL_TEST", False) is False, reason="Functional, requires credentials.")
class TestFunctionalFlows(object):
    """
    Functional tests which check the OpenID, Auth flows using real credentials.

    Overview:

    1) Dynamic registration
    2) Obtain an token to represent a TPP using the client credential flow.
    3) Using access token to make an account request with the correct permissions.
    4) Initiate a flow using the account request ID.

    """

    @classmethod
    def setup_class(cls):
        """Test you can on-board with each ASPSP that supports registration & client_ids for account requests."""
        # Dynamic registration.
        cls.clients = []
        for bank in model_banks:
            ob = OpenBanking(
                bank=bank['name'],
                signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
                signing_public=os.environ.get('SIGNING_PUBLIC'),
                transport_key=os.environ.get('TRANSPORT_PRIVATE'),
                transport_public=os.environ.get('TRANSPORT_PUBLIC'),
            )
            response = ob.client_registration(ssa=os.environ.get('SSA'))
            assert response.status_code is 200 or response.status_code is 201
            assert response.response.get("client_id")
            # Store client id for use in client credentials etc .
            cls.clients.append(dict(
                name=bank['name'],
                financial_id=bank['financial_id'],
                client_id=response.response.get("client_id")

            ))
            print("res======")
            print(response.response)
            assert 1 is 2
    @classmethod
    def teardown_class(cls):
        """ Teardown deregister with each ASPSP that supports it."""

        # Deregister with each bank if possible
        for bank in model_banks:
            if not bank['name'] == "Ozone":  # TODO: Ozone does not support deregister :(
                ob = OpenBanking(
                    bank=bank['name'],
                    signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
                    signing_public=os.environ.get('SIGNING_PUBLIC'),
                    transport_key=os.environ.get('TRANSPORT_PRIVATE'),
                    transport_public=os.environ.get('TRANSPORT_PUBLIC'),
                )
                response = ob.client_deregistration(ssa=os.environ.get('SSA'))
                assert int(response.status_code) is 200

    def setup_module(module):
        """ setup any state specific to the execution of the given module."""
        pass

    def teardown_module(module):
        """ teardown any state that was previously setup with a setup_module method."""
        pass

    @pytest.mark.run(order=1)
    def test_grant_client_credentials(self):
        """
        Test that a TPP can obtain an access token from each ASPSP using the client credential flow.

        Pre-requisites:
            - Prior to calling the API, the TPP must registered with the ASPSP.

        Test Acceptance criteria:
            - Each ASPSP returns a successful response status code 201
            - Each ASPSP responds with a unique 'account_token' that can be used in the account_request grant


        """
        for client in self.clients:
            ob = OpenBanking(
                bank=client['name'],
                signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
                signing_public=os.environ.get('SIGNING_PUBLIC'),
                transport_key=os.environ.get('TRANSPORT_PRIVATE'),
                transport_public=os.environ.get('TRANSPORT_PUBLIC'),
            )
            # Note: OB specs omit openid in scopes, but it can be added and should be ignore.
            response = ob.grant_client_credentials(client_id=client['client_id'], scope="openid accounts payments")
            assert response.status_code is 200
            # TODO:  Successful Response should return access_token, token_type and expires_in
            # TODO: See https://tools.ietf.org/html/rfc6749#section-5.1
            assert response.response.get("access_token") and len(response.response.get("access_token")) > 1
            # Store the access token for account request.
            client['account_token'] = response.response.get("access_token")

    @pytest.mark.run(order=2)
    def test_account_request(self):
        """
        Test that each TPP can use the access token obtained in client credential to obtain an `AccountRequestId`.

        Pre-requisites:
            - Prior to calling the API, the TPP must have an access token issued by the ASPSP
              using a client credentials grant in test: `test_grant_client_credentials`.

        Test Acceptance criteria:
            - Each ASPSP returns a successful response status code 201
            - Each ASPSP responds with a unique AccountRequestId that refers to the resource.

        """

        for client in self.clients:
            ob = OpenBanking(
                bank=client['name'],
                signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
                signing_public=os.environ.get('SIGNING_PUBLIC'),
                transport_key=os.environ.get('TRANSPORT_PRIVATE'),
                transport_public=os.environ.get('TRANSPORT_PUBLIC'),
            )

            permissions = [
                "ReadAccountsDetail",
                "ReadBalances",
                "ReadBeneficiariesDetail",
                "ReadDirectDebits",
                "ReadProducts",
                "ReadStandingOrdersDetail",
                "ReadTransactionsCredits",
                "ReadTransactionsDebits",
                "ReadTransactionsDetail"
            ]
            response = ob.account_transaction_requests(access_token=client['account_token'], permissions=permissions)
            assert response.status_code is 201
            assert response.response["Data"]['AccountRequestId'] and len(
                response.response["Data"]['AccountRequestId']) > 1
            # Store unique AccountRequestId.
            client['AccountRequestId'] = response.response["Data"]['AccountRequestId']

    # @pytest.mark.run(order=3)
    # def test_account_status_awaiting_authorisation(self):
    #     """
    #     Check that a TPP can retrieve an account-request resource that they have created to check its status.
    #
    #     Pre-requisites:
    #         - Prior to calling the API, the TPP must have an AccountRequestId test: `test_account_request`.
    #
    #     Test Acceptance criteria:
    #         - Each ASPSP returns a Account Request Status 200.
    #         - Each ASPSP responds with a response body `AwaitingAuthorisation`.
    #
    #     """""
    #     for client in self.clients:
    #         ob = OpenBanking(
    #             bank=client['name'],
    #             signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
    #             signing_public=os.environ.get('SIGNING_PUBLIC'),
    #             transport_key=os.environ.get('TRANSPORT_PRIVATE'),
    #             transport_public=os.environ.get('TRANSPORT_PUBLIC'),
    #         )
    #         response = ob.account_requests_status(account_request_id=client['AccountRequestId'],
    #                                               access_token=client['account_token'])
    #         print(client['name'])
    #         print(response.status_code)
    #         print(response.response)
    #         assert response.status_code is 201
    #         assert response.response == "AwaitingAuthorisation"

    # @pytest.mark.run(order=4)
    # def test_account_request_delete(self):
    #     """
    #
    #      Check that a TPP can delete the account-request resource with the ASPSP before confirming consent revocation with the PSU.
    #
    #      Pre-requisites:
    #         - Prior to calling the API, the AISP must have an access token issued by the ASPSP
    #           using a client credentials grant in test: `test_account_request`.
    #
    #     Test Acceptance criteria:
    #         - Each ASPSP returns a successful delete response status code ?s
    #
    #     """
    #     #TODO move this to leg only do once, clean up before auth of account.
    #     for client in self.clients:
    #         ob = OpenBanking(
    #             bank=client['name'],
    #             signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
    #             signing_public=os.environ.get('SIGNING_PUBLIC'),
    #             transport_key=os.environ.get('TRANSPORT_PRIVATE'),
    #             transport_public=os.environ.get('TRANSPORT_PUBLIC'),
    #         )
    #         response = ob.account_requests_delete(account_request_id=client['AccountRequestId'],
    #                                               access_token=client['account_token'])
    #         print(client['name'])
    #         print(response.status_code)
    #         print(response.response)
    #         assert response.status_code is 201
    #

    @pytest.mark.run(order=5)
    def test_psu_hybrid_flow(self, pytestconfig):
        """
        Test you can initiate the hybrid flow in order to get an access token that contains the user's approval.

        You must complete the following steps as the TPP:

        Pre-requisites:
            - Prior to calling the API, the TPP must have an *Valid* AccountRequestId test: `test_account_request`.

        Test Acceptance criteria:
            - Generate a request parameter JWT.
            - Fake redirection of the PSU to the ASPSP authorization endpoint.
            - Receive the authorization code response via a call back.

        """

        for client in self.clients:
            ob = OpenBanking(
                bank=client['name'],
                signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
                signing_public=os.environ.get('SIGNING_PUBLIC'),
                transport_key=os.environ.get('TRANSPORT_PRIVATE'),
                transport_public=os.environ.get('TRANSPORT_PUBLIC'),
            )

            url = ob.make_hybrid_psu_url(
                client_id=client['client_id'],
                account_id=client['AccountRequestId']
            )

            # Open web browser and wait for the psu to complete the hybrid flow.
            webbrowser.open_new(url)
            # If the PSU grants the request we will receive a response via the redirect URI and input it here.
            psu_input_code, psu_input_id_token = get_psu_input_code(pytestconfig, client['name'])
            assert psu_input_code
            client['authorization_code'] = psu_input_code

    @pytest.mark.run(order=6)
    def test_exchange_authorization_code(self, pytestconfig):
        """
        Test that you can exchange the authorization code for an access token.

        Pre-requisites:
            - Prior to calling the API, PSU grants the request and the TPP receives a response via the redirect URI.

        Test Acceptance criteria:
            - Exchange the authorization code for an access token.

        """

        for client in self.clients:
            ob = OpenBanking(
                bank=client['name'],
                signing_key=os.environ.get('SIGNING_KEY_PRIVATE'),
                signing_public=os.environ.get('SIGNING_PUBLIC'),
                transport_key=os.environ.get('TRANSPORT_PRIVATE'),
                transport_public=os.environ.get('TRANSPORT_PUBLIC'),
            )
            response = ob.get_access_token(authorization_code=client['authorization_code'],
                                           iss=client['client_id'],
                                           sub=client['client_id']
                                           )

            assert response.status_code is 200
            assert response.response["access_token"]
            assert response.response["expires_in"]
            assert response.response["scope"]
            assert response.response["id_token"] and len(response.response["id_token"]) > 1
            client['access_token'] = response.response["access_token"]
            # display_something(pytestconfig, client['access_token'])

    @pytest.mark.run(order=7)
    def test_account_endpoints(self):
        """
        Test the Account Information API.

        Pre-requisites:
            - Prior to calling the API, you should have a valid access token as a result of the hybrid flow above.
            - This access token represents user consent for account information access.

        Test Acceptance criteria:
            - Have data.

        """
        for client in self.clients:
            assert client['access_token']

            ob = OpenBanking(
                bank="Ozone",
                access_token=client['access_token'],
                transport_key=os.environ.get('TRANSPORT_PRIVATE'),
                transport_public=os.environ.get('TRANSPORT_PUBLIC'),
            )

            accounts = ob.accounts()
            assert accounts.raw_data
            assert accounts.raw_data.get('Data') != None

