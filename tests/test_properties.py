"""
Tests class properties such as session etc.
"""
import os
import pytest
from openbanking import OpenBanking
from openbanking.config import model_banks
from openbanking.exceptions import ConfigurationException


def test_property_authorization_endpoint():
    for bank in model_banks:
        ob = OpenBanking(bank=bank['name'])
        assert ob.well_known_authorization

def test_property_registration_endpoint():
    for bank in model_banks:
        ob = OpenBanking(bank=bank['name'])
        assert ob.well_known_registration


def test_resource_server_endpoint():
    for bank in model_banks:
        endpoint = bank['resource_server']
        if endpoint:
            ob = OpenBanking(bank=bank['name'])
            assert ob.resource_server

def test_property_financial_id_with_credentials():
    """Test you can return the financial_id when setting config bank name."""
    for bank in model_banks:
        ob = OpenBanking(bank=bank['name'])
        financial_id = ob.financial_id
        assert financial_id




# Kid
@pytest.mark.skipif(os.environ.get("LOCAL_TEST", False) is False, reason="Functional, requires credentials.")
def test_property_kid():
    """Test you get can return the kid, SHA-1 Thumbprint of the signing certificate."""

    for bank in model_banks:
        ob = OpenBanking(signing_public=os.environ.get("SIGNING_PUBLIC"), bank=bank['name'],)
        assert ob.kid

# def test_property_kid_without_credentials():
#     """Test trying to get kid  without setting the public key credential raises a ConfigurationException."""
#     ob = OpenBanking(bank="ForgeRock")
#     with pytest.raises(ConfigurationException):
#         print(ob.kid)

