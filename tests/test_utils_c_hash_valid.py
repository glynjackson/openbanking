import pytest
from openbanking.utils import is_c_hash_valid
from openbanking.exceptions import AlgorithmError


class TestIsCHashValidFunction(object):
    """A number of unit tests for `~openbanking.utils.is_c_hash_valid()`"""

    id_token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImlpRzh6UEJ4ZGJKQTFqdzN6ejMxcmJ4RldTayJ9.eyJzdWIiOiJjOThhYTczOC0yN2U5LTRhZDktYmQ1My0yMGFmMTMyMzlhOWYiLCJvcGVuYmFua2luZ19pbnRlbnRfaWQiOiJjOThhYTczOC0yN2U5LTRhZDktYmQ1My0yMGFmMTMyMzlhOWYiLCJpc3MiOiJodHRwczovL21vZGVsb2JhbmthdXRoMjAxOC5vM2JhbmsuY28udWs6NDEwMSIsImF1ZCI6ImM0NzJkYzdiLWU0NjEtNGMyNS04ZGM3LTEyMzVhMzMyODk1NCIsImlhdCI6MTUzMTk4NzQ1NywiZXhwIjoxNTMxOTkxMDU3LCJub25jZSI6IjVhNmIwZDc4MzJhOWZiNGY4MGYxMTcwYSIsImNfaGFzaCI6Im1WMzlBMmtNbGhHN0xfMHZnc3c3enc9PSJ9.MbZW_brEI-TpP2hkSBpBzW5KFK8Q_Qa3xtZSCXEXdHQ9Brh53ie-Af8RKPJ6DKPnffn2y2ff76KN0CrEyndHAZIq7JjZiD4ZdWm_PcTROKSzdv696wraRCW3OSuFJWUVBtx_npLYOROIXWfAH3zOrHyXlmiSP9WJ3_Im9nD4y0Y"
    code = "8b6b3cd6-1ea2-44e3-8681-a0d9e7f0a1dc"

    def test_utils_c_hash_valid_returns_true(self):
        """
        Test that the function returns True if the encoded value of code and c_hash in the claims of the id_token match.

         Pre-requisites:
            - Must have a valid input id_token and code string.

        Test Acceptance criteria:
            - 'is_c_hash_valid()' can be invoked with no error and returns is True.
            - 'is_c_hash_valid()' returns boolean type

        """

        result = is_c_hash_valid(self.code, self.id_token)
        assert type(result) is bool
        assert result is True

    def test_utils_c_hash_valid_returns_false(self):
        """
        Test that an invalid c_hash match from a encoded token fails if it doesn't match.

        Pre-requisites:
            - An encoded code token that does not match the `c_hash` value in the claims id_token.

        Test Acceptance criteria:
            - 'is_c_hash_valid()' returns boolean value False.

        """
        bad_id_token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImlpRzh6UEJ4ZGJKQTFqdzN6ejMxcmJ4RldTayJ9.eyJzdWIiOiJjOThhYTczOC0yN2U5LTRhZDktYmQ1My0yMGFmMTMyMzlhOWYiLCJvcGVuYmFua2luZ19pbnRlbnRfaWQiOiJjOThhYTczOC0yN2U5LTRhZDktYmQ1My0yMGFmMTMyMzlhOWYiLCJpc3MiOiJodHRwczovL21vZGVsb2JhbmthdXRoMjAxOC5vM2JhbmsuY28udWs6NDEwMSIsImF1ZCI6ImM0NzJkYzdiLWU0NjEtNGMyNS04ZGM3LTEyMzVhMzMyODk1NCIsImlhdCI6MTUzMTk4NzQ1NywiZXhwIjoxNTMxOTkxMDU3LCJub25jZSI6IjVhNmIwZDc4MzJhOWZiNGY4MGYxMTcwYSIsImNfaGFzaCI6Im1WMzlBMmtNbGhHN0xfMHZnc3c3ejI9PSJ9.VxIcJP-4aIvIYXnYVGEt5vNEIuH2vLvNd6sDzy7um4L9wpjgWMaD8vCZIXKE0JaZOXPnHcezFrZTMxSfGG2HbfgY7ZOSuHTjlWYCIC16CihXBtlOmU7t0CqSzCT67xLQAARtG0muqcWp0UqX3hDF0z5bdNdrNo58dhHRQ1a2NrA"
        result = is_c_hash_valid(self.code, bad_id_token)
        assert result is False

    def test_utils_c_hash_algorithm_error(self):
        """
        Test that an invalid alg from a encoded token raises an error:

        Pre-requisites:
            - An encoded code token and a bad alg claims in id_token.

        Test Acceptance criteria:
            - 'is_c_hash_valid()' returns boolean value False.

        """

        bad_id_token ="eyJhbGciOiJQUzM4NCIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.oywIg-I6w59yw9jiPxewn5n2BhrD7hSifWSmzFKGBMPEMd0qweVNjlyxu2TodunPzlh49OW8QA0ygNRL9VQrWA3GXzb5FubNF4s7Y15QePx52anlvebzihx5-hR0UhKbVC0UODwYNMiY-v0L7iMbT9UvuSj0GAuZMxndo2Y2VFQ"
        with pytest.raises(AlgorithmError):
            is_c_hash_valid(self.code, bad_id_token)



